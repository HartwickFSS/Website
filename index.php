<?php

$err = true; //Set true for error reporting

if ($err && !($_SERVER['HTTP_HOST'] == "www.hartwickfss.org")) { // Disable error reporting in live
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

$content = '';

$preScripts = '';

if (key($_GET) == "Resources") {

  $title = 'Resources';

  $content = file_get_contents('pages/Resources.html');

} elseif (key($_GET) == "Licensing") {

  $title = 'Licensing';

  $content = file_get_contents('pages/Licensing.html');

} elseif (key($_GET) == "Support") {

  $title = 'Support';

  $content = file_get_contents('pages/Support.html');

} elseif (key($_GET) == "Schedule") {

  $title = 'Schedule of Events';

  $preScripts = PHP_EOL . '    ' . '<link rel="stylesheet" type="text/css" media="screen" href="css/schedule.css" />';

  include('calendar.php');

  $pre_content = file_get_contents('pages/Schedule.html');

  $content = str_replace('{CALENDAR}', $calendarContent, $pre_content);


} elseif (key($_GET) == "Get_Involved") {

  $title = 'Get Involved';

  $content = file_get_contents('pages/Get_Involved.html');

} elseif (key($_GET) == "Source") {

  $title = 'Source';

  $content = file_get_contents('pages/Source.html');

} elseif (key($_GET) == "Privacy_Policy") {

  $title = 'Privacy Policy';

  $content = file_get_contents('pages/Privacy_Policy.html');

} elseif (key($_GET) == "Contact") {

  $title = 'Contact';

  $content = file_get_contents('pages/Contact.html');

} elseif (key($_GET) == "Freedom") {

  $title = 'Freedom';

  $content = file_get_contents('pages/Freedom.html');

} else {

  $title = '';

  $RSSFeed = '';

  $RSSFeed = file_get_contents('pages/News.html');

  $pre_content = file_get_contents('pages/Home.html');

  $content = str_replace('{NEWS}', $RSSFeed, $pre_content);

}

$config = array(
  'indent'          => true,
  'output-xhtml'    => true,
  'show-body-only'  => true,
  'wrap'            => false,
);

$tidy = new tidy;
$tidy->parseString($content, $config, 'utf8');
$tidy->repairString('');

function ifGet($title) {
  if (strlen($title) >= 1) {
    return '?' . $title;
  }
}

function insertTabs($content) {
  $result = '';
  $separator = "\r\n";
  $line = strtok($content, $separator);

  while ($line !== false) {
      $line = strtok($separator);
      $result .= '        ' . $line . PHP_EOL;
  }
  return $result;
}

$c = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html id="root" xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <title>';

if (strlen($title) >= 1) { $c .= $title . ' - '; }

$c .= 'Hartwick Free Software Society</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="theme-color" content="#222222" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <link rel="shortcut icon" type="image/png" href="images/icon.png" />
    <link rel="icon" type="image/png" href="images/icon.png" />' . $preScripts . '
  </head>
  <body>
    <div id="container">
      <div id="header">
        <div><img src="images/HartwickFSS-Logo-HawkOnly.svg" alt="Hartwick Free Software Society Logo" /></div>
        <div>
          <h1>Hartwick</h1>
          <h2>Free Software Society</h2>
          <h3><a href="https://maps.google.com?daddr=Hartwick+College,+Oneonta,+NY+13820&amp;layer=t" onclick="target=\'_blank\'">Hartwick College, Oneonta NY</a></h3>
        </div>
        <div>
          <p id="consultation" class="bubble you"><strong>Note:</strong> This Organization is not <br />yet officially recognized by the <br />Hartwick College Student Senate.</p>
        </div>
      </div>
      <span id="telephone" onmouseover="phoneNumOn();" onmouseout="phoneTextOn();">&#9742; <a href="tel:+19853779425" id="phoneText" style="display: inline;">(985) FSS - WICK</a><a href="" id="phoneNum" style="display: none;">(985) 377 - 9425</a></span>
      <div id="m_navbar"><span onclick="toggleMenu();">&#9776; Menu</span></div>
      <div id="navbar" style="left: -100vw; -webkit-transition: left 0.5s;">
        <ul>
          <li><a href="/">Home</a></li>
          <li'; if ($title == "Resources") { $c .= ' class="active"'; } $c .= '><a href="/?Resources">Resources</a></li>
          <li'; if ($title == "Licensing") { $c .= ' class="active"'; } $c .= '><a href="/?Licensing">Licensing</a></li>
          <li'; if ($title == "Support") { $c .= ' class="active"'; } $c .= '><a href="/?Support">Support</a></li>
          <li'; if ($title == "Schedule") { $c .= ' class="active"'; } $c .= '><a href="/?Schedule">Schedule of Events</a></li>
          <li class="last'; if ($title == "Get_Involved") { $c .= ' active'; } $c .= '"><a href="/?Get_involved">Get Involved</a></li>
        </ul>
      </div>
      <hr />

      <div id="content">' . PHP_EOL . PHP_EOL . '<div>' . PHP_EOL . insertTabs($tidy) . '      ' . '</div>

    </div>
    <div class="reset"></div>
    <div id="footer">
			<div id="footerContainer">
				<span id="copy">Copyright &copy; ' . date('Y') . ' Hartwick Free Software Society | <a href="/?License">Source Code, Licenses, &amp; Attribution</a> | <a href="/?Privacy_Policy">Privacy Policy</a> | <a href="/?Contact">Contact Information</a></span>
			</div>
		</div>
  </body>
  <script type="text/javascript">
      function toggleMenu() {
        if (document.getElementById("navbar").style.left == "-100vw") {
          document.getElementById("navbar").style.left = "0vw";
        } else {
          document.getElementById("navbar").style.left = "-100vw";
        }
      }
      function phoneNumOn() {
				document.getElementById("phoneText").style.display = "none";
				document.getElementById("phoneNum").style.display = "inline";
      }
      function phoneTextOn() {
				document.getElementById("phoneText").style.display = "inline";
				document.getElementById("phoneNum").style.display = "none";
      }
    </script>
</html>
<!-- Web Development by Brian J. Hodge; www.brianjhodge.com -->';

echo $c;

?>
