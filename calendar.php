<?php

$calendarContent = PHP_EOL . '';

$event = '';

$dayOffset = 1;

$lastDayOfLastMonth = 31;

$daysInThisMonth = 31;

$countDown = ($lastDayOfLastMonth - $dayOffset) + 1;

$dayCount = 0;

$todayDayNum = date('j');

$events[24] = [
  'title'   => 'Move-In Day',
  'type'    => 'hartwick',
  'description' => 'Students may begin moving in. Full schedule of events at <a href="http://www.hartwick.edu/wp-content/uploads/2016/08/CSSWelcomeWeekendBrochure2016.pdf" onclick="target=\'_blank\'">http://www.hartwick.edu/wp-content/uploads/2016/08/CSSWelcomeWeekendBrochure2016.pdf</a>.'
];
/*
$events[26] = [
  'title'   => '6p Semi-Weekly Meeting',
  'type'    => 'fss',
  'description' => '06:00 PM - Thursday August 25th, 2016<br />Yager Hall Room 101<br />Opening Covocation'
];
*/
$events[25] = [
  'title'   => '10a Opening Convocation',
  'type'    => 'hartwick',
  'description' => '10:00 AM - Thursday August 25th, 2016<br />Opening Covocation'
];

$events[29] = [
  'title'   => 'Classes Begin',
  'type'    => 'hartwick',
  'description' => 'Monday August 29th, 2016<br />Start of Classes'
];

for ($week = 1; $week <= 6; $week++) {
  for ($day = 1; $day <= 7; $day++) {
    $class = '';
    if ($dayOffset > 0) {
      $dayNum = $countDown;
      $class = ' ' . 'grey';
    } else {
      $dayCount++;
      $dayNum = $dayCount;
      if (isset($events[$dayNum])) {
        $event = '<div class="event ' . $events[$dayNum]['type'] . '"><div class="hideOnMobile">' . $events[$dayNum]['title'] . '</div><div class="showOnMobile plus">&#43;</div></div>';
      } else {
        $event = '';
      }
    }
    if ($dayCount > $daysInThisMonth) {
        $dayNum = '';
    }
    if ($dayNum == $todayDayNum) {
      $class .= ' ' . 'today';
    }
    $calendarContent .= '<div class="week_' . $week . ' day_' . $day . ' dayBox' . $class . '">' . $dayNum . $event . '</div>';
    $dayOffset--;
  }
}

?>
