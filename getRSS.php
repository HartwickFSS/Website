<?php

$err = false;

if ($err) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

$RSSFeed = '';

$rss = new DOMDocument();
$rss->load('https://static.fsf.org/fsforg/rss/news.xml');
$feed = array();
foreach ($rss->getElementsByTagName('item') as $node) {
  $item = array (
    'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
    'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
    'date' => $node->getElementsByTagName('date')->item(0)->nodeValue,
    );
  array_push($feed, $item);
}
$limit = 5;
for($x=0;$x<$limit;$x++) {
  $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
  $link = $feed[$x]['link'];
  $date = date('l F d, Y', strtotime($feed[$x]['date']));
  $RSSFeed .= '<p><strong><a href="'.$link.'" title="'.$title.'" onclick="target=\'_blank\'">'.$title.'</a></strong><br />';
  $RSSFeed .= '<small>'.$date.'</small></p>';
}

file_put_contents('pages/News.html', $RSSFeed);

?>
